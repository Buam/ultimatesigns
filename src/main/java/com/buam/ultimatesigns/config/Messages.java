package com.buam.ultimatesigns.config;

import com.buam.ultimatesigns.utils.TextUtils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Messages {

    public static Messages i;

    private File configFile;
    private FileConfiguration config;

    public Messages(final JavaPlugin plugin) {
        i = this;
        createConfig(plugin);
        readMessages();
    }

    public String sd(final String key, final String d) {
        String s = config.getString(key, d);

        if(s == null) return "";

        return TextUtils.translateColors(s);
    }

    public String s(final String key, final Object... args) {
        String s = config.getString(key);

        if(s == null) return "";

        for(int i = 0; i < args.length; i++) {
            s = s.replace("%" + (i + 1), args[i].toString());
        }

        return TextUtils.translateColors(s);
    }

    private void createConfig(final JavaPlugin plugin) {
        configFile = new File(plugin.getDataFolder(), "messages.yml");
        if(!configFile.exists()) {
            if(!configFile.getParentFile().mkdirs()) System.out.println(ChatColor.RED + "[UltimateSigns] failed to create messages.yml");
            plugin.saveResource("messages.yml", false);
        }

        config = new YamlConfiguration();
        try {
            config.load(configFile);
            addNewDefaults();
        } catch(IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void addNewDefaults() throws IOException {
        if(!config.isSet("no-permission-add-cmd-message")) config.addDefault("no-permission-add-cmd-message", "&cYou don't have permission to add that command");
        config.options().copyDefaults(true);
        config.save(configFile);
    }

    public static String NO_PERMISSION_SIGN = "&cYou don't have permission to use this sign!";
    public static String NO_PERMISSION = "&cYou don't have permission to use that command!";
    public static String NO_PERMISSION_ADD_CMD = "&cYou don't have permission to add that command";
    public static String NOT_YOUR_SIGN = "&cYou don't have permission to break that sign!";
    public static String CANCEL = "&cCancelled";
    public static String VALID_NUMBER = "&cPlease enter a valid number";
    public static String LOOK_AT_SIGN = "&cLook at a sign to use this command";
    public static String ONLY_PLAYERS = "&cOnly for players!";

    public static String ADD_COMMAND = "Type the command you want to add into the chat";
    public static String REMOVE_COMMAND = "Which command do you want to remove? (0 to cancel)";
    public static String EDIT_COMMAND = "Which command do you want to edit?";
    public static String ADDED_COMMAND = "added-command-message";
    public static String REMOVED_COMMAND = "removed-command-message";
    public static String CHANGED_COMMAND = "changed-command-message";
    public static String ENTER_NEW_COMMAND = "Enter the new command";
    public static String SIGN_NO_COMMAND = "sign-no-command-message";
    public static String SIGN_NO_COMMANDS = "&cThis sign has no commands";
    public static String SIGN_COMMANDS = "This sign has the following commands:";

    public static String ADD_PERMISSION = "Type the permission you want into the chat";
    public static String REMOVE_PERMISSION = "Which permission do you want to remove? (0 to cancel)";
    public static String EDIT_PERMISSION = "Which permission do you want to edit?";
    public static String ADDED_PERMISSION = "added-permission-message";
    public static String REMOVED_PERMISSION = "removed-permission-message";
    public static String CHANGED_PERMISSION = "changed-permission-message";
    public static String ENTER_NEW_PERMISSION = "Enter the new permission";
    public static String SIGN_NO_PERMISSION = "sign-no-permission-message";
    public static String SIGN_NO_PERMISSIONS = "&cThis sign does not require any permissions";
    public static String SIGN_PERMISSIONS = "This sign requires the following permissions:";

    public static String NO_SIGN_EDITOR = "&cThe sign editor is not available";
    public static String SIGN_EDITOR_ENABLED = "&aSign editing mode enabled";
    public static String SIGN_EDITOR_DISABLED = "&cSign editing mode disabled";

    public static String COPIED_SIGN = "Copied sign";
    public static String PASTED_SIGN = "Pasted sign";
    public static String COPY_FIRST = "&cCopy a sign first";
    public static String SAME_SIGN = "&cChoose a different sign to paste";

    public static String LOADED_COMMANDS = "Successfully loaded commands from file!";
    public static String LOADING_ERROR = "Something went wrong while loading the file:";

    public static String RELOADED = "Reloaded successfully";


    private void readMessages() {
        NO_PERMISSION_SIGN = sd("no-permission-sign-message", NO_PERMISSION_SIGN);
        NO_PERMISSION = sd("no-permission-message", NO_PERMISSION);
        NO_PERMISSION_ADD_CMD = sd("no-permission-add-cmd-message", NO_PERMISSION_ADD_CMD);
        NOT_YOUR_SIGN = sd("not-your-sign-message", NOT_YOUR_SIGN);
        CANCEL = sd("cancel-message", CANCEL);
        VALID_NUMBER = sd("valid-number-message", VALID_NUMBER);
        LOOK_AT_SIGN = sd("look-at-sign-message", LOOK_AT_SIGN);
        ONLY_PLAYERS = sd("only-players-message", ONLY_PLAYERS);

        ADD_COMMAND = sd("add-command-message", ADD_COMMAND);
        REMOVE_COMMAND = sd("remove-command-message", REMOVE_COMMAND);
        EDIT_COMMAND = sd("edit-command-message", EDIT_COMMAND);
        ENTER_NEW_COMMAND = sd("enter-new-command-message", ENTER_NEW_COMMAND);
        SIGN_NO_COMMANDS = sd("sign-no-commands-message", SIGN_NO_COMMANDS);
        SIGN_COMMANDS = sd("sign-commands-message", SIGN_COMMANDS);

        ADD_PERMISSION = sd("add-permission-message", ADD_PERMISSION);
        REMOVE_PERMISSION = sd("remove-permission-message", REMOVE_PERMISSION);
        EDIT_PERMISSION = sd("edit-permission-message", EDIT_PERMISSION);
        ENTER_NEW_PERMISSION = sd("enter-new-permission-message", ENTER_NEW_PERMISSION);
        SIGN_NO_PERMISSIONS = sd("sign-no-permissions-message", SIGN_NO_PERMISSIONS);
        SIGN_PERMISSIONS = sd("sign-permissions-message", SIGN_PERMISSIONS);

        NO_SIGN_EDITOR = sd("no-sign-editor-message", NO_SIGN_EDITOR);
        SIGN_EDITOR_ENABLED = sd("sign-editor-enabled-message", SIGN_EDITOR_ENABLED);
        SIGN_EDITOR_DISABLED = sd("sign-editor-disabled-message", SIGN_EDITOR_DISABLED);

        COPIED_SIGN = sd("copied-sign-message", COPIED_SIGN);
        PASTED_SIGN = sd("pasted-sign-message", PASTED_SIGN);
        COPY_FIRST = sd("copy-first-message", COPY_FIRST);
        SAME_SIGN = sd("same-sign-message", SAME_SIGN);

        LOADED_COMMANDS = sd("loaded-commands-message", LOADED_COMMANDS);
        LOADING_ERROR = sd("loading-error-message", LOADING_ERROR);

        RELOADED = sd("reloaded-message", RELOADED);
    }

}
