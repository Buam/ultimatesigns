package com.buam.ultimatesigns.commands.cmd.sub;

import com.buam.ultimatesigns.SharedConstants;
import com.buam.ultimatesigns.SignManager;
import com.buam.ultimatesigns.UltimateSigns;
import com.buam.ultimatesigns.commands.ChatStates;
import com.buam.ultimatesigns.commands.ParticleHelper;
import com.buam.ultimatesigns.commands.SignState;
import com.buam.ultimatesigns.commands.cmd.CMDBase;
import com.buam.ultimatesigns.config.Messages;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class CMDEdit implements Listener {

    public void onCommand(final Player player) {
        Block target = player.getTargetBlockExact(40);

        // If it is not a sign, don't do anything
        if(!SharedConstants.isSign(target.getType())) {
            player.sendMessage(UltimateSigns.PREFIX + Messages.LOOK_AT_SIGN);
            return;
        }

        // If the sign is not yet registered, do so
        if(SignManager.i.isUltimateSign(target.getLocation())) SignManager.i.addSign(target.getLocation(), player.getUniqueId());

        // Does this sign have commands? listCommands() handles error
        if(CMDBase.listCommands(player, target)) {
            // Set state
            UltimateSigns.command.states.put(player, new SignState(target, ChatStates.EDIT_COMMAND_INDEX));

            player.sendMessage(UltimateSigns.PREFIX + Messages.EDIT_COMMAND);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player player = e.getPlayer();

        if(UltimateSigns.command.states.containsKey(player)) {
            SignState state = UltimateSigns.command.states.get(player);
            String message = e.getMessage().trim();
            if (message.equals("0")) {
                // Abort
                player.sendMessage(UltimateSigns.PREFIX + Messages.CANCEL);

                UltimateSigns.command.states.remove(player);
            } else {
                if (state.state == ChatStates.EDIT_COMMAND_INDEX) {
                    // This player typed in the index of the command to edit

                    // Parse integer and go on with the new command (new state)
                    try {
                        int index = Integer.parseInt(message) - 1;

                        if (SignManager.i.hasCommand(state.block.getLocation(), index)) {
                            // There is a command with that index, new state with index
                            state.state = ChatStates.EDIT_COMMAND;
                            state.index = index;
                            UltimateSigns.command.states.put(player, state);

                            player.sendMessage(UltimateSigns.PREFIX + Messages.ENTER_NEW_COMMAND);
                        } else {
                            player.sendMessage(UltimateSigns.PREFIX + Messages.i.s(Messages.SIGN_NO_COMMAND, index));
                        }

                    } catch (NumberFormatException exc) {
                        player.sendMessage(UltimateSigns.PREFIX + Messages.VALID_NUMBER);
                    }
                } else if (state.state == ChatStates.EDIT_COMMAND) {

                    String oldCommand = SignManager.i.signAt(state.block.getLocation()).getCommands().get(state.index);

                    // This player typed in the new command, the state now has an index assigned
                    SignManager.i.editCommand(state.block.getLocation(), state.index, message);

                    player.sendMessage(UltimateSigns.PREFIX + Messages.i.s(Messages.CHANGED_COMMAND, oldCommand, message));

                    // Remove the player from the states list, he's done
                    UltimateSigns.command.states.remove(player);

                    // Play a cool particle effect!
                    ParticleHelper.p(state.block.getLocation());
                }
            }

            e.setCancelled(true);
        }

    }

}
