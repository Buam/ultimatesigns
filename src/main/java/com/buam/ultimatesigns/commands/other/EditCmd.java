package com.buam.ultimatesigns.commands.other;

import com.buam.ultimatesigns.SharedConstants;
import com.buam.ultimatesigns.UltimateSigns;
import com.buam.ultimatesigns.config.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
/ultimatesigns edit
Includes all arguments, gets created and called when args[0] = "edit"
 */
public class EditCmd {

    public void onCommand(CommandSender sender) {
        // Not a sub command for the console so:
        if(sender instanceof Player) {
            Player player = (Player) sender;

            if(!player.hasPermission(SharedConstants.EDIT_PERMISSION)) {
                player.sendMessage(Messages.NO_PERMISSION);
                return;
            }

            if(UltimateSigns.command.inEditMode.contains(player)) {
                UltimateSigns.command.inEditMode.remove(player);
                player.sendMessage(UltimateSigns.PREFIX + Messages.SIGN_EDITOR_DISABLED);
            } else {
                UltimateSigns.command.inEditMode.add(player);
                player.sendMessage(UltimateSigns.PREFIX + Messages.SIGN_EDITOR_ENABLED);
            }
        } else {
            sender.sendMessage(Messages.ONLY_PLAYERS);
        }

    }

}
