package com.buam.ultimatesigns.commands.other;

import com.buam.ultimatesigns.SharedConstants;
import com.buam.ultimatesigns.SignManager;
import com.buam.ultimatesigns.UltimateSigns;
import com.buam.ultimatesigns.config.Messages;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class CopyPasteCmd {

    final Map<Player, Block> copied = new HashMap<>();

    public void onCommand(final CommandSender sender, final String[] args) {
        // Get player
        if(sender instanceof Player) {
            Player player = (Player) sender;

            if(!player.hasPermission(SharedConstants.COPY_PERMISSION)) {
                player.sendMessage(Messages.NO_PERMISSION);
                return;
            }

            // See if it is a valid sign
            Block target = player.getTargetBlockExact(40);

            // If it is not a sign, don't do anything
            if(!SharedConstants.isSign(target.getType())) {
                player.sendMessage(UltimateSigns.PREFIX + Messages.LOOK_AT_SIGN);
                return;
            }

            // If the sign is not yet registered, do so
            if(SignManager.i.isUltimateSign(target.getLocation())) SignManager.i.addSign(target.getLocation(), player.getUniqueId());

            if(args[0].equals("copy")) {
                // Copy the sign
                copied.put(player, target);
                player.sendMessage(UltimateSigns.PREFIX + Messages.COPIED_SIGN);
            } else {
                // Paste the sign
                Block original = copied.get(player);

                if(original == null) {
                    // Player hasn't copied yet
                    player.sendMessage(UltimateSigns.PREFIX + Messages.COPY_FIRST);
                    return;
                }

                if(original.getLocation().equals(target.getLocation())) {
                    // The blocks are the same, cancel
                    player.sendMessage(UltimateSigns.PREFIX + Messages.SAME_SIGN);
                    return;
                }

                // Copy the sign
                SignManager.i.setSign(target, original);
                player.sendMessage(UltimateSigns.PREFIX + Messages.PASTED_SIGN);
            }
        } else {
            sender.sendMessage(Messages.ONLY_PLAYERS);
        }

    }

}
